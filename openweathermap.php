<?php

class OpenWeatherMapItem{
	public $clima;
	public $clima_descripcion;
	public $clima_icon;
	public $fecha_hora;
	public $temperatura;
	public $temperatura_max;
	public $temperatura_min;

	public function getClima(){
		return $this->clima;
	}
	public function setClima($valor){
		$this->clima=$valor;
	}
	public function getClimaDescripcion(){
		return $this->clima_descripcion;
	}
	public function setClimaDescripcion($valor){
		$this->clima_descripcion=$valor;
	}
	public function getClimaIcon(){
		return $this->clima_icon;
	}
	public function setClimaIcon($valor){
		$this->clima_icon=$valor;
	}
	public function getFechaHora(){
		return $this->fecha_hora;
	}
	public function setFechaHora($valor){
		$this->fecha_hora=$valor;
	}
	public function getTemperatura(){
		return $this->temperatura;
	}
	public function setTemperatura($valor){
		$this->temperatura=$valor;
	}

	public function getDia(){
		return date('d', strtotime($this->getFechaHora()));
	}
	public function getDiaDeLaSemana(){
		$dia= date('w', strtotime($this->getFechaHora()));
		$dias_de_la_semana=["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"];	
		return $dias_de_la_semana[$dia];
	}
	public function getMesDelYear(){
		$meses = [
			'Enero',
			'Febrero',
			'Marzo',
			'Abril',
			'Mayo',
			'Junio',
			'Julio',
			'Agosto',
			'Septiembre',
			'Octubre',
			'Noviembre',
			'Diciembre'
		];
		$mes= date('m', strtotime($this->getFechaHora()));
		return $meses[$mes];
	}
	public function getClimaDescripcionEsp(){
		$res=$this->getClimaDescripcion();
		switch($res){
			case "light rain":
				$res="Lluvia suave";
				break;
			case "snow":
				$res="Nieve";
				break;
			case "moderate rain":
				$res="Lluvia moderada";
				break;
			default:
				break;
		}
		return $res;
	}
	public function getFechaVisible(){
		$res=$this->getDiaDeLaSemana();
		$res.=", ".date('d', strtotime($this->getFechaHora()));
		//$res.=" de ".date('m', strtotime($this->getFechaHora()));
		$res.=" de ".$this->getMesDelYear();
		$res.=" de ".date('Y', strtotime($this->getFechaHora()));
		return $res;
	}
	public function getTemperaturaCelsius(){
		return round($this->temperatura-(273.15),1);
	}
	public function getTemperaturaMaxCelsius(){
		return round($this->temperatura_max-(273.15),1);
	}
	public function getTemperaturaMinCelsius(){
		return round($this->temperatura_min-(273.15),1);
	}
	function htmlGetIcon(){
		return "<img src='".(get_stylesheet_directory_uri()."/imagenes/weather/".$this->getClimaIcon().".png")."'>";
	}
}
class OpenWeatherMap{
	private $app_id="[[YOUR ID]]";
	private $ciudad="";
	private $habitantes="";
	private $temperaturas;
	private $temperatura_ahora;
	

	public function getCiudad(){
		return $this->ciudad;
	}
	public function setCiudad($valor){
		$this->ciudad=$valor;
	}
	public function getAppId(){
		return $this->app_id;
	}
	public function setAppId($valor){
		$this->app_id=$valor;
	}
	public function getTemperaturas(){
		return $this->temperaturas;
	}
	public function getHabitantes(){
		return $this->habitantes;
	}
	public function setHabitantes($valor){
		$this->habitantes=$valor;
	}

	function bindDataWeather($data){
		$list=json_decode($data,true);
		$this->temperaturas=[];
		for($i=0;$i<count($list["list"]);$i++){
			$item= new OpenWeatherMapItem();
			$item->clima=$list["list"][$i]["weather"][0]["main"];
			$item->clima_descripcion=$list["list"][$i]["weather"][0]["description"];
			$item->clima_icon=$list["list"][$i]["weather"][0]["icon"];
			$item->fecha_hora=$list["list"][$i]["dt_txt"];
			$item->temperatura=$list["list"][$i]["main"]["temp"];
			$item->temperatura_max=$list["list"][$i]["main"]["temp_max"];
			$item->temperatura_min=$list["list"][$i]["main"]["temp_min"];
			array_push($this->temperaturas,$item);
		}
		$this->setHabitantes($list["city"]["population"]);
	}
	function getDataApi($url){
		return file_get_contents($url);
	}
	function initForecastApi($url){
		$data=$this->getDataApi($url);
		$this->bindDataWeather($data);
	}

	function getForecastGranada() {
		$res=Array();
		$this->setCiudad("Granada");
		$this->getForecast();
		return $res;
	}
	function getForecast($p_ciudad=""){
		if($p_ciudad!=""){
			$this->setCiudad("Granada");
		}
		$url=$this->getForecastUrl();
		$this->initForecastApi($url);
		echo $this->htmlGetHoy();
		echo $this->htmlGetProximosDias();
	}
	function getForecastUrl(){
		$res="";
		$url_forecast="http://api.openweathermap.org/data/2.5/forecast";
		$res=$url_forecast.'?q='.$this->getCiudad().'&appid='.$this->getAppId();

		return $res;
	}
	function getHoraActual(){
		$tz_object = new DateTimeZone('Europe/Madrid');
		$datetime = new DateTime();
		$datetime->setTimezone($tz_object);
		return $datetime->format('h:i');
	}
	function getWeatherGranada() {
		$res=Array();
		$this->setCiudad("Granada");
		$this->getWeather();
		return $res;
	}
	function getWeather($p_ciudad=""){
		if($p_ciudad!=""){
			$this->setCiudad("Granada");
		}
		$url=$this->getWeathertUrl();
		//$this->initApi($url);
	}
	function getWeatherUrl(){
		$res="";
		$url_weather="http://api.openweathermap.org/data/2.5/weather";
		$res=$url_weather.'?q='.$this->getCiudad().'&appid='.$this->getAppId();

		return $res;
	}
	function htmlGetHoy(){
		$res="
			<div class='weather-hoy'>
				<div class='weather-hoy-nombre'>".$this->getCiudad()."</div>
				<div class='weather-hoy-fecha'>".$this->getTemperaturas()[0]->getFechaVisible()."</div>
				<div class='weather-hoy-hora'>".$this->getHoraActual()."</div>
				<div class='weather-hoy-climadescripcion'>".$this->getTemperaturas()[0]->getClimaDescripcionEsp()."</div>
				<div class='weather-hoy-temperaturas'>
					<div class='weather-hoy-icono'>".$this->getTemperaturas()[0]->htmlGetIcon()."</div>
					<div class='weather-hoy-temperatura'>".$this->getTemperaturas()[0]->getTemperaturaCelsius()." ºC</div>
				</div>
			</div>
		";
		return $res;
	}
	function htmlGetProximosDias(){
		$res="<div class='weatherlist flex-col'>";
		$condicional_dia=false;
		for($i=0;$i<count($this->getTemperaturas());$i++){
			if($this->getTemperaturas()[0]->getDia()!=$this->getTemperaturas()[$i]->getDia()){
				if($this->getTemperaturas()[($i-1)]->getDia()<$this->getTemperaturas()[$i]->getDia()){
					$condicional_dia=true;
				}
			}
			if($condicional_dia){
				$condicional_dia=false;
				$res.="
				<div class='weatherlist-item flex-col5'>
					<div class='weatherlist-item-fecha '>".$this->getTemperaturas()[$i]->getDiaDeLaSemana()."</div>
					<div class='weatherlist-item-icon '>".$this->getTemperaturas()[$i]->htmlGetIcon()."</div>
					<div class='weatherlist-item-temperaturas'>
						<div class='weatherlist-item-temperatura-max'>".$this->getTemperaturas()[$i]->getTemperaturaCelsius()." ºC</div>
					</div>
				</div>";
			}
		}
		$res.="</div>";
		return $res;
	}
}
?>