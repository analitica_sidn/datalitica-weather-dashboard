# Datalitica - Weather Dashboard

Dahsboard para mostrar los datos del tiempo de hoy y los próximos 5 días.
Este dashboard se alimenta de la herramienta gratuíta https://openweathermap.org/.
Para hacer uso del código se ha de cambiar el valor de: $app_id  y llamar al método getForecast()

La clase todavía no contempla el método getWeather.